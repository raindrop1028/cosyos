;*******************************************************************************
;* @item     CosyOS-II Config
;* @file     mcucfg_80251.inc
;* @brief    80251 Core Config File
;* @detail   自定义任务切换现场保护，汇编语言保护方案。
;* @author   迟凯峰
;* @version  V3.0.0
;* @date     2024.06.25
;*******************************************************************************

; 下方各定义项已经直接给出示例（保护/恢复：DPH1、DPL1、DPS），直接修改即可。

DPS		DATA	0E3H	; 定义相关寄存器
DPL1	DATA	0E4H
DPH1	DATA	0E5H

mUserReg_PUSH	MACRO	; 定义保护现场代码
		PUSH	DPH1
		PUSH	DPL1
		PUSH	DPS
				ENDM

mUserReg_POP	MACRO	; 定义恢复现场代码
		POP		DPS
		POP		DPL1
		POP		DPH1
				ENDM
