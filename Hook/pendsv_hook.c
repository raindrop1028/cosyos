/**************************************************************************//**
 * @item     CosyOS-II Hook
 * @file     pendsv_hook.c
 * @brief    挂起服务钩子
 * @detail   用于处理标志队列中的中断挂起服务。
 * @author   迟凯峰
 * @version  V3.0.0
 * @date     2024.06.25
 ******************************************************************************/

#include "..\System\os_link.h"
#if SYSCFG_PENDSVHOOK == __ENABLED__

void pendsv_hook(void) MCUCFG_C51USING
{
	
}

#endif
