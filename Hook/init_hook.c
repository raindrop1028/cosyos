/**************************************************************************//**
 * @item     CosyOS-II Hook
 * @file     init_hook.c
 * @brief    初始化钩子
 * @detail   在主函数中首先被调用，适用于初始化时钟、GPIO、寄存器等工作。
 * @author   迟凯峰
 * @version  V3.0.0
 * @date     2024.06.25
 ******************************************************************************/

#include "..\System\os_link.h"

void init_hook(void)
{
	
}
