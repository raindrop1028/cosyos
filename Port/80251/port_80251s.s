;*******************************************************************************
;* @item     CosyOS-II Port
;* @file     port_80251s.s
;* @brief    80251 Core Port File
;* @author   迟凯峰
;* @version  V3.0.1
;* @date     2024.07.07
;*******************************************************************************
;
$INCLUDE (..\Config\syscfg.h)
IF SYSCFG_MCUCORE == 80251
$INCLUDE (..\Config\mcucfg_80251.h)
$INCLUDE (..\Config\mcucfg_80251.inc)
;
;///////////////////////////////////////////////////////////////////////////////
;
; 用户定义寄存器
;
; 1、mcucfg_80251.h -> PendSV中断配置 -> 中断清零，涉及的寄存器；
; 2、用户自定义任务切换现场保护，汇编语言保护方案，涉及的寄存器；
;
; 通常，直接包含标准头文件（h、inc）即可。
; 如果包含h文件导致编译报错，说明h文件中存在汇编器不能识别的C语言语法，
; 您可包含inc文件或重写新的h文件，也可在此处直接定义相关寄存器。
;
TCON    DATA    088H
IE0     BIT     TCON^1
;
;///////////////////////////////////////////////////////////////////////////////

				NAME	PORT_80251S

;///////////////////////////////////////////////////////////////////////////////

$IF ROMHUGE		; Code Rom Size: Huge
Prefix	LIT '?'
IF MCUCFG_NOOVERLAY
Prepar	LIT '?'
ELSE
Prepar	LIT '??'
ENDIF
PRSeg	LIT 'ECODE'
PRJmp	LIT 'EJMP'
PRCall	LIT 'ECALL'
PRRet	LIT 'ERET'
$ELSE			; Code Rom Size: Large
Prefix  LIT ''
IF MCUCFG_NOOVERLAY
Prepar	LIT ''
ELSE
Prepar	LIT '?_'
ENDIF
PRSeg	LIT 'CODE'
PRJmp	LIT 'LJMP'
PRCall	LIT 'LCALL'
PRRet	LIT 'RET'
$ENDIF

DPL		DATA	082H
DPH		DATA	083H
DPXL	DATA	084H
PSW		DATA	0D0H
PSW1	DATA	0D1H

; OS_PendSV_Handler
; PendSV中断服务函数

?PR?OS_PendSV_Handler{Prefix}?PORT_80251S	SEGMENT	PRSeg
?STACK			SEGMENT	EDATA
				EXTRN	PRSeg(sPendSV_Handler{Prefix})
				EXTRN	DATA (s_task_current)
				EXTRN	DATA (s_task_news)
				EXTRN	DATA (m_bsp_add)
				IF SYSCFG_DEBUGGING == __ENABLED__
				EXTRN	BIT  (s_sign_taskmgr)
				EXTRN	DATA (s_taskstacklen)
				ENDIF
				IF SYSCFG_TASKPC_MONITOR == __ENABLED__
				EXTRN	DATA (s_pc)
				ENDIF

				CSEG	AT	MCUCFG_PENDSV_VECTORADDR
				PRJmp	OS_PendSV_Handler{Prefix}

				RSEG	?PR?OS_PendSV_Handler{Prefix}?PORT_80251S
OS_PendSV_Handler{Prefix}:

				PUSH	DR28
				PUSH	DR24
				PUSH	DR20
				PUSH	DR16
				PUSH	DR12
				PUSH	DR8
				PUSH	DR4
				PUSH	DR0
				PUSH	DR56
				PUSH	PSW1
				PUSH	PSW

				mPendSV_Clear
				; 任务栈长
IF SYSCFG_DEBUGGING == __ENABLED__
				CLR		C
				MOV		DR8, DR60
	IF MCUCFG_TASKSTACK_MODE == __MSP__
				SUB		WR10, #WORD0 (?STACK-1)
	ELSE
				MOV		WR6, s_task_current
				ADD		WR6, m_bsp_add
				MOV		WR4, @WR6
				DEC		WR4, #1
				SUB		WR10, WR4
	ENDIF
	IF MCUCFG_USERREGSIZE
				ADD		WR10, #MCUCFG_USERREGSIZE
	ENDIF
				MOV		s_taskstacklen, WR10
ENDIF
				PRCall	sPendSV_Handler{Prefix}

				CLR		C
				JNZ		?PROTECTING
				LJMP	RETURN
?PROTECTING:	DEC		A
				JNZ		TASKPC
				LJMP	RESTORE

TASKPC:			; 任务PC
IF SYSCFG_TASKPC_MONITOR == __ENABLED__
				JNB		s_sign_taskmgr, PROTECTING
				MOV		DR8, DR60
				SUB		WR10, #42-1
				MOV		WR4, @WR10
				MOV		R7, @WR10+2
				MOV		R6, @WR10+3
				MOV		R4, #0
				MOV		s_pc, DR4
ENDIF

PROTECTING:		; 保护现场
IF MCUCFG_USERREGSIZE
				mUserReg_PUSH
ENDIF

				MOV		DR8, DR60
IF MCUCFG_MEMORYMODEL == 0	; 方案一
				MOV		WR6, s_task_current
				MOV		@WR6, WR10
ELSE
				MOV		WR8, #0
				SUB		WR10, #WORD0 (?STACK-1)
				MOV		WR2, WR10
				SRL		WR2
				SRL		WR2
	IF MCUCFG_MEMORYMODEL == 1	; 方案二
	?REG	LIT 'WR6'
				MOV		WR6, s_task_current
				MOV		@WR6, WR10
				ADD		WR6, m_bsp_add
				MOV		WR6, @WR6
				ADD		WR6, WR10
				ANL		WR10, #3
				JZ		SAVESTACK
				INC		WR2, #1
				INC		DR60, #4
				SUB		DR60, DR8
				INC		WR6, #4
				SUB		WR6, WR10
	ELSEIF MCUCFG_MEMORYMODEL == 2 || MCUCFG_MEMORYMODEL == 3	; 方案三 & 方案四
	?REG	LIT 'DR28'
				MOV		DR4, s_task_current
				MOV		@DR4, WR10
				MOV		WR12, #0
				MOV		WR14, m_bsp_add
				ADD		DR4, DR12
				MOV		WR28, @DR4
				MOV		WR30, @DR4+2
				ADD		DR28, DR8
				ANL		WR10, #3
				JZ		SAVESTACK
				INC		WR2, #1
				INC		DR60, #4
				SUB		DR60, DR8
				INC		DR28, #4
				SUB		DR28, DR8
	ELSEIF MCUCFG_MEMORYMODEL == 4	; 方案五
	?REG	LIT 'DR4'
				MOV		R4, #0
				MOV		R5, DPXL
				MOV		WR6, s_task_current
				MOV		@DR4, WR10
				ADD		WR6, m_bsp_add
				MOV		WR6, @DR4
				ADD		WR6, WR10
				ANL		WR10, #3
				JZ		SAVESTACK
				INC		WR2, #1
				INC		DR60, #4
				SUB		DR60, DR8
				INC		WR6, #4
				SUB		WR6, WR10
	ENDIF

SAVESTACK:
	IF MCUCFG_TASKSWITCHMODE == 1
				POP		DR12
				MOV		@?REG-1*4+0, WR12
				MOV		@?REG-1*4+2, WR14
				POP		DR12
				MOV		@?REG-2*4+0, WR12
				MOV		@?REG-2*4+2, WR14
				POP		DR12
				MOV		@?REG-3*4+0, WR12
				MOV		@?REG-3*4+2, WR14
				POP		DR12
				MOV		@?REG-4*4+0, WR12
				MOV		@?REG-4*4+2, WR14
				POP		DR12
				MOV		@?REG-5*4+0, WR12
				MOV		@?REG-5*4+2, WR14
				POP		DR12
				MOV		@?REG-6*4+0, WR12
				MOV		@?REG-6*4+2, WR14
				POP		DR12
				MOV		@?REG-7*4+0, WR12
				MOV		@?REG-7*4+2, WR14
				POP		DR12
				MOV		@?REG-8*4+0, WR12
				MOV		@?REG-8*4+2, WR14
				POP		DR12
				MOV		@?REG-9*4+0, WR12
				MOV		@?REG-9*4+2, WR14
				POP		DR12
				MOV		@?REG-10*4+0, WR12
				MOV		@?REG-10*4+2, WR14
				SUB		?REG, #10*4
				SUB		WR2, #10
	ENDIF
SAVELOOP:		POP		DR12
				DEC		?REG, #4
				MOV		@?REG+0, WR12
				MOV		@?REG+2, WR14
				DEC		WR2, #1
				JNE		SAVELOOP
ENDIF

RESTORE:		; 恢复现场
IF MCUCFG_MEMORYMODEL == 0	; 方案一
				MOV		WR6, s_task_news
				MOV		s_task_current, WR6
				MOV		WR10, @WR6
				MOV		DR60, DR8
ELSE
				MOV		DR60, #WORD0 (?STACK-1)
	IF MCUCFG_MEMORYMODEL == 1	; 方案二
				MOV		WR6, s_task_news
				MOV		s_task_current, WR6
				MOV		WR10, @WR6
				ADD		WR6, m_bsp_add
				MOV		WR6, @WR6
	ELSEIF MCUCFG_MEMORYMODEL == 2 || MCUCFG_MEMORYMODEL == 3	; 方案三 & 方案四
				MOV		DR4, s_task_news
				MOV		s_task_current, DR4
				MOV		WR10, @DR4
				MOV		WR12, #0
				MOV		WR14, m_bsp_add
				ADD		DR4, DR12
				MOV		WR28, @DR4
				MOV		WR30, @DR4+2
	ELSEIF MCUCFG_MEMORYMODEL == 4	; 方案五
				MOV		R4, #0
				MOV		R5, DPXL
				MOV		WR6, s_task_news
				MOV		s_task_current, WR6
				MOV		WR10, @DR4
				ADD		WR6, m_bsp_add
				MOV		WR6, @DR4
	ENDIF
				MOV		WR2, WR10
				SRL		WR2
				SRL		WR2
				ANL		WR10, #3
				JZ		RESTORESTACK
				INC		WR2, #1

RESTORESTACK:
	IF MCUCFG_TASKSWITCHMODE == 1
				MOV		WR12, @?REG+0*4+0
				MOV		WR14, @?REG+0*4+2
				PUSH	DR12
				MOV		WR12, @?REG+1*4+0
				MOV		WR14, @?REG+1*4+2
				PUSH	DR12
				MOV		WR12, @?REG+2*4+0
				MOV		WR14, @?REG+2*4+2
				PUSH	DR12
				MOV		WR12, @?REG+3*4+0
				MOV		WR14, @?REG+3*4+2
				PUSH	DR12
				MOV		WR12, @?REG+4*4+0
				MOV		WR14, @?REG+4*4+2
				PUSH	DR12
				MOV		WR12, @?REG+5*4+0
				MOV		WR14, @?REG+5*4+2
				PUSH	DR12
				MOV		WR12, @?REG+6*4+0
				MOV		WR14, @?REG+6*4+2
				PUSH	DR12
				MOV		WR12, @?REG+7*4+0
				MOV		WR14, @?REG+7*4+2
				PUSH	DR12
				MOV		WR12, @?REG+8*4+0
				MOV		WR14, @?REG+8*4+2
				PUSH	DR12
				MOV		WR12, @?REG+9*4+0
				MOV		WR14, @?REG+9*4+2
				PUSH	DR12
				ADD		?REG, #10*4
				SUB		WR2, #10
	ENDIF
RESTORELOOP:	MOV		WR12, @?REG+0
				MOV		WR14, @?REG+2
				PUSH	DR12
				INC		?REG, #4
				DEC		WR2, #1
				JNE		RESTORELOOP

				JZ		USERREGPOP
				MOV		WR8, #0
				DEC		DR60, #4
				ADD		DR60, DR8
ENDIF

USERREGPOP:
IF MCUCFG_USERREGSIZE
				mUserReg_POP
ENDIF

RETURN:			POP		PSW
				POP		PSW1
				POP		DR56
				POP		DR0
				POP		DR4
				POP		DR8
				POP		DR12
				POP		DR16
				POP		DR20
				POP		DR24
				POP		DR28

				RETI

;///////////////////////////////////////////////////////////////////////////////

; 中断挂起服务FIFO队列

IF MCUCFG_PENDSVFIFO_DEPTH > 0

; mPendSV_FIFOPreempt
; 抢占队列项

?PR?mPendSV_FIFOPreempt{Prefix}?PORT_80251S	SEGMENT	PRSeg
				EXTRN	BIT  (m_sign_fifo)
				EXTRN	BIT  (m_sign_fifo_0_0)
				EXTRN	BIT  (m_sign_fifo_0_1)
				EXTRN	BIT  (m_sign_fifo_0_2)
				EXTRN	BIT  (m_sign_fifo_0_3)
				EXTRN	BIT  (m_sign_fifo_0_4)
				EXTRN	BIT  (m_sign_fifo_0_5)
				EXTRN	BIT  (m_sign_fifo_0_6)
				EXTRN	BIT  (m_sign_fifo_0_7)
				EXTRN	BIT  (m_sign_fifo_1_0)
				EXTRN	BIT  (m_sign_fifo_1_1)
				EXTRN	BIT  (m_sign_fifo_1_2)
				EXTRN	BIT  (m_sign_fifo_1_3)
				EXTRN	BIT  (m_sign_fifo_1_4)
				EXTRN	BIT  (m_sign_fifo_1_5)
				EXTRN	BIT  (m_sign_fifo_1_6)
				EXTRN	BIT  (m_sign_fifo_1_7)
IF MCUCFG_PENDSVFIFO_DEPTH > 8
				EXTRN	BIT  (m_sign_fifo_0_8)
				EXTRN	BIT  (m_sign_fifo_0_9)
				EXTRN	BIT  (m_sign_fifo_0_10)
				EXTRN	BIT  (m_sign_fifo_0_11)
				EXTRN	BIT  (m_sign_fifo_0_12)
				EXTRN	BIT  (m_sign_fifo_0_13)
				EXTRN	BIT  (m_sign_fifo_0_14)
				EXTRN	BIT  (m_sign_fifo_0_15)
				EXTRN	BIT  (m_sign_fifo_1_8)
				EXTRN	BIT  (m_sign_fifo_1_9)
				EXTRN	BIT  (m_sign_fifo_1_10)
				EXTRN	BIT  (m_sign_fifo_1_11)
				EXTRN	BIT  (m_sign_fifo_1_12)
				EXTRN	BIT  (m_sign_fifo_1_13)
				EXTRN	BIT  (m_sign_fifo_1_14)
				EXTRN	BIT  (m_sign_fifo_1_15)
IF MCUCFG_PENDSVFIFO_DEPTH > 16
				EXTRN	BIT  (m_sign_fifo_0_16)
				EXTRN	BIT  (m_sign_fifo_0_17)
				EXTRN	BIT  (m_sign_fifo_0_18)
				EXTRN	BIT  (m_sign_fifo_0_19)
				EXTRN	BIT  (m_sign_fifo_0_20)
				EXTRN	BIT  (m_sign_fifo_0_21)
				EXTRN	BIT  (m_sign_fifo_0_22)
				EXTRN	BIT  (m_sign_fifo_0_23)
				EXTRN	BIT  (m_sign_fifo_1_16)
				EXTRN	BIT  (m_sign_fifo_1_17)
				EXTRN	BIT  (m_sign_fifo_1_18)
				EXTRN	BIT  (m_sign_fifo_1_19)
				EXTRN	BIT  (m_sign_fifo_1_20)
				EXTRN	BIT  (m_sign_fifo_1_21)
				EXTRN	BIT  (m_sign_fifo_1_22)
				EXTRN	BIT  (m_sign_fifo_1_23)
IF MCUCFG_PENDSVFIFO_DEPTH > 24
				EXTRN	BIT  (m_sign_fifo_0_24)
				EXTRN	BIT  (m_sign_fifo_0_25)
				EXTRN	BIT  (m_sign_fifo_0_26)
				EXTRN	BIT  (m_sign_fifo_0_27)
				EXTRN	BIT  (m_sign_fifo_0_28)
				EXTRN	BIT  (m_sign_fifo_0_29)
				EXTRN	BIT  (m_sign_fifo_0_30)
				EXTRN	BIT  (m_sign_fifo_0_31)
				EXTRN	BIT  (m_sign_fifo_1_24)
				EXTRN	BIT  (m_sign_fifo_1_25)
				EXTRN	BIT  (m_sign_fifo_1_26)
				EXTRN	BIT  (m_sign_fifo_1_27)
				EXTRN	BIT  (m_sign_fifo_1_28)
				EXTRN	BIT  (m_sign_fifo_1_29)
				EXTRN	BIT  (m_sign_fifo_1_30)
				EXTRN	BIT  (m_sign_fifo_1_31)
IF MCUCFG_PENDSVFIFO_DEPTH > 32
				EXTRN	BIT  (m_sign_fifo_0_32)
				EXTRN	BIT  (m_sign_fifo_0_33)
				EXTRN	BIT  (m_sign_fifo_0_34)
				EXTRN	BIT  (m_sign_fifo_0_35)
				EXTRN	BIT  (m_sign_fifo_0_36)
				EXTRN	BIT  (m_sign_fifo_0_37)
				EXTRN	BIT  (m_sign_fifo_0_38)
				EXTRN	BIT  (m_sign_fifo_0_39)
				EXTRN	BIT  (m_sign_fifo_1_32)
				EXTRN	BIT  (m_sign_fifo_1_33)
				EXTRN	BIT  (m_sign_fifo_1_34)
				EXTRN	BIT  (m_sign_fifo_1_35)
				EXTRN	BIT  (m_sign_fifo_1_36)
				EXTRN	BIT  (m_sign_fifo_1_37)
				EXTRN	BIT  (m_sign_fifo_1_38)
				EXTRN	BIT  (m_sign_fifo_1_39)
ENDIF
ENDIF
ENDIF
ENDIF
				RSEG	?PR?mPendSV_FIFOPreempt{Prefix}?PORT_80251S
mPendSV_FIFOPreempt{Prefix}:

IF MCUCFG_MEMORYMODEL <= 2
				MOV		DR0, #0x0100
ELSE
				MOV		R0, #0
				MOV		DPTR, #0x0100
ENDIF
				JB		m_sign_fifo, FIFO_0
IF MCUCFG_MEMORYMODEL <= 2
				INC		R2
ELSE
				INC		DPH
ENDIF
				LJMP	FIFO_1

FIFO_0:			JBC		m_sign_fifo_0_0,  FIFO_X_0
				JBC		m_sign_fifo_0_1,  FIFO_X_1
				JBC		m_sign_fifo_0_2,  FIFO_X_2
				JBC		m_sign_fifo_0_3,  FIFO_X_3
				JBC		m_sign_fifo_0_4,  FIFO_X_4
				JBC		m_sign_fifo_0_5,  FIFO_X_5
				JBC		m_sign_fifo_0_6,  FIFO_X_6
				JBC		m_sign_fifo_0_7,  FIFO_X_7
IF MCUCFG_PENDSVFIFO_DEPTH > 8
				JBC		m_sign_fifo_0_8,  FIFO_X_8
				JBC		m_sign_fifo_0_9,  FIFO_X_9
				JBC		m_sign_fifo_0_10, FIFO_X_10
				JBC		m_sign_fifo_0_11, FIFO_X_11
				JBC		m_sign_fifo_0_12, FIFO_X_12
				JBC		m_sign_fifo_0_13, FIFO_X_13
				JBC		m_sign_fifo_0_14, FIFO_X_14
				JBC		m_sign_fifo_0_15, FIFO_X_15
IF MCUCFG_PENDSVFIFO_DEPTH > 16
				JBC		m_sign_fifo_0_16, FIFO_X_16
				JBC		m_sign_fifo_0_17, FIFO_X_17
				JBC		m_sign_fifo_0_18, FIFO_X_18
				JBC		m_sign_fifo_0_19, FIFO_X_19
				JBC		m_sign_fifo_0_20, FIFO_X_20
				JBC		m_sign_fifo_0_21, FIFO_X_21
				JBC		m_sign_fifo_0_22, FIFO_X_22
				JBC		m_sign_fifo_0_23, FIFO_X_23
IF MCUCFG_PENDSVFIFO_DEPTH > 24
				JBC		m_sign_fifo_0_24, FIFO_X_24
				JBC		m_sign_fifo_0_25, FIFO_X_25
				JBC		m_sign_fifo_0_26, FIFO_X_26
				JBC		m_sign_fifo_0_27, FIFO_X_27
				JBC		m_sign_fifo_0_28, FIFO_X_28
				JBC		m_sign_fifo_0_29, FIFO_X_29
				JBC		m_sign_fifo_0_30, FIFO_X_30
				JBC		m_sign_fifo_0_31, FIFO_X_31
IF MCUCFG_PENDSVFIFO_DEPTH > 32
				JBC		m_sign_fifo_0_32, FIFO_X_32
				JBC		m_sign_fifo_0_33, FIFO_X_33
				JBC		m_sign_fifo_0_34, FIFO_X_34
				JBC		m_sign_fifo_0_35, FIFO_X_35
				JBC		m_sign_fifo_0_36, FIFO_X_36
				JBC		m_sign_fifo_0_37, FIFO_X_37
				JBC		m_sign_fifo_0_38, FIFO_X_38
				JBC		m_sign_fifo_0_39, FIFO_X_39
ENDIF
ENDIF
ENDIF
ENDIF
				INC		R0
				PRRet

FIFO_X_0:		MOV		A, #0*2
				PRRet
FIFO_X_1:		MOV		A, #1*2
				PRRet
FIFO_X_2:		MOV		A, #2*2
				PRRet
FIFO_X_3:		MOV		A, #3*2
				PRRet
FIFO_X_4:		MOV		A, #4*2
				PRRet
FIFO_X_5:		MOV		A, #5*2
				PRRet
FIFO_X_6:		MOV		A, #6*2
				PRRet
FIFO_X_7:		MOV		A, #7*2
				PRRet
IF MCUCFG_PENDSVFIFO_DEPTH > 8
FIFO_X_8:		MOV		A, #8*2
				PRRet
FIFO_X_9:		MOV		A, #9*2
				PRRet
FIFO_X_10:		MOV		A, #10*2
				PRRet
FIFO_X_11:		MOV		A, #11*2
				PRRet
FIFO_X_12:		MOV		A, #12*2
				PRRet
FIFO_X_13:		MOV		A, #13*2
				PRRet
FIFO_X_14:		MOV		A, #14*2
				PRRet
FIFO_X_15:		MOV		A, #15*2
				PRRet
IF MCUCFG_PENDSVFIFO_DEPTH > 16
FIFO_X_16:		MOV		A, #16*2
				PRRet
FIFO_X_17:		MOV		A, #17*2
				PRRet
FIFO_X_18:		MOV		A, #18*2
				PRRet
FIFO_X_19:		MOV		A, #19*2
				PRRet
FIFO_X_20:		MOV		A, #20*2
				PRRet
FIFO_X_21:		MOV		A, #21*2
				PRRet
FIFO_X_22:		MOV		A, #22*2
				PRRet
FIFO_X_23:		MOV		A, #23*2
				PRRet
IF MCUCFG_PENDSVFIFO_DEPTH > 24
FIFO_X_24:		MOV		A, #24*2
				PRRet
FIFO_X_25:		MOV		A, #25*2
				PRRet
FIFO_X_26:		MOV		A, #26*2
				PRRet
FIFO_X_27:		MOV		A, #27*2
				PRRet
FIFO_X_28:		MOV		A, #28*2
				PRRet
FIFO_X_29:		MOV		A, #29*2
				PRRet
FIFO_X_30:		MOV		A, #30*2
				PRRet
FIFO_X_31:		MOV		A, #31*2
				PRRet
IF MCUCFG_PENDSVFIFO_DEPTH > 32
FIFO_X_32:		MOV		A, #32*2
				PRRet
FIFO_X_33:		MOV		A, #33*2
				PRRet
FIFO_X_34:		MOV		A, #34*2
				PRRet
FIFO_X_35:		MOV		A, #35*2
				PRRet
FIFO_X_36:		MOV		A, #36*2
				PRRet
FIFO_X_37:		MOV		A, #37*2
				PRRet
FIFO_X_38:		MOV		A, #38*2
				PRRet
FIFO_X_39:		MOV		A, #39*2
				PRRet
ENDIF
ENDIF
ENDIF
ENDIF

FIFO_1:			JBC		m_sign_fifo_1_0,  FIFO_X_0
				JBC		m_sign_fifo_1_1,  FIFO_X_1
				JBC		m_sign_fifo_1_2,  FIFO_X_2
				JBC		m_sign_fifo_1_3,  FIFO_X_3
				JBC		m_sign_fifo_1_4,  FIFO_X_4
				JBC		m_sign_fifo_1_5,  FIFO_X_5
				JBC		m_sign_fifo_1_6,  FIFO_X_6
				JBC		m_sign_fifo_1_7,  FIFO_X_7
IF MCUCFG_PENDSVFIFO_DEPTH > 8
				JBC		m_sign_fifo_1_8,  FIFO_X_8
				JBC		m_sign_fifo_1_9,  FIFO_X_9
				JBC		m_sign_fifo_1_10, FIFO_X_10
				JBC		m_sign_fifo_1_11, FIFO_X_11
				JBC		m_sign_fifo_1_12, FIFO_X_12
				JBC		m_sign_fifo_1_13, FIFO_X_13
				JBC		m_sign_fifo_1_14, FIFO_X_14
				JBC		m_sign_fifo_1_15, FIFO_X_15
IF MCUCFG_PENDSVFIFO_DEPTH > 16
				JBC		m_sign_fifo_1_16, FIFO_X_16
				JBC		m_sign_fifo_1_17, FIFO_X_17
				JBC		m_sign_fifo_1_18, FIFO_X_18
				JBC		m_sign_fifo_1_19, FIFO_X_19
				JBC		m_sign_fifo_1_20, FIFO_X_20
				JBC		m_sign_fifo_1_21, FIFO_X_21
				JBC		m_sign_fifo_1_22, FIFO_X_22
				JBC		m_sign_fifo_1_23, FIFO_X_23
IF MCUCFG_PENDSVFIFO_DEPTH > 24
				JBC		m_sign_fifo_1_24, FIFO_X_24
				JBC		m_sign_fifo_1_25, FIFO_X_25
				JBC		m_sign_fifo_1_26, FIFO_X_26
				JBC		m_sign_fifo_1_27, FIFO_X_27
				JBC		m_sign_fifo_1_28, FIFO_X_28
				JBC		m_sign_fifo_1_29, FIFO_X_29
				JBC		m_sign_fifo_1_30, FIFO_X_30
				JBC		m_sign_fifo_1_31, FIFO_X_31
IF MCUCFG_PENDSVFIFO_DEPTH > 32
				JBC		m_sign_fifo_1_32, FIFO_X_32
				JBC		m_sign_fifo_1_33, FIFO_X_33
				JBC		m_sign_fifo_1_34, FIFO_X_34
				JBC		m_sign_fifo_1_35, FIFO_X_35
				JBC		m_sign_fifo_1_36, FIFO_X_36
				JBC		m_sign_fifo_1_37, FIFO_X_37
				JBC		m_sign_fifo_1_38, FIFO_X_38
				JBC		m_sign_fifo_1_39, FIFO_X_39
ENDIF
ENDIF
ENDIF
ENDIF
				INC		R0
				PRRet

; mPendSV_FIFOLoader
; 装载中断挂起服务的结构体指针

?PR?mPendSV_FIFOLoader{Prepar}?PORT_80251S	SEGMENT	PRSeg
				PUBLIC	mPendSV_FIFOLoader{Prepar}
				RSEG	?PR?mPendSV_FIFOLoader{Prepar}?PORT_80251S
mPendSV_FIFOLoader{Prepar}:

				PRCall	mPendSV_FIFOPreempt{Prefix}
IF MCUCFG_MEMORYMODEL <= 2
				MOV		R3, A
ELSE
				MOV		DPL, A
ENDIF
				MOV		A, R0
				JNZ		FAILED
IF MCUCFG_MEMORYMODEL <= 2
				MOV		@WR2, WR6
ELSE
				MOV		@DR56, WR6
ENDIF
FAILED:			PRRet

ENDIF

;///////////////////////////////////////////////////////////////////////////////

ENDIF
				END
