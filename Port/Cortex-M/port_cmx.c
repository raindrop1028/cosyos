/**************************************************************************//**
 * @item     CosyOS-II Port
 * @file     port_cmx.c
 * @brief    CMSIS Cortex-M Core Port File
 * @author   迟凯峰
 * @version  V3.0.0
 * @date     2024.06.25
 ******************************************************************************/

#include "..\System\os_var.h"
#ifdef __PORT_CMX_H

s_u32_t  m_basepri = 1;
#if SYSCFG_TASKMSG == __ENABLED__
s_u32_t *m_taskmsg_psp;
#endif



/*
 * 中断挂起服务FIFO队列
 */

#if MCUCFG_PENDSVFIFO_DEPTH > 0

#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
s_u32_t mPendSV_FIFO_DepthMAX = 0;
#endif
void *mPendSV_FIFO[2][MCUCFG_PENDSVFIFO_DEPTH + 1]; /*!< 中断挂起服务FIFO队列：FIFO0、FIFO1 */
#if MCUCFG_PENDSVFIFO_MUTEX == 0
register void **mPendSV_FIFO_P0 __ASM("r10");       /*!< FIFO0指针-全局寄存器变量 */
register void **mPendSV_FIFO_P1 __ASM("r11");       /*!< FIFO1指针-全局寄存器变量 */
#else
void **mPendSV_FIFO_P0;                             /*!< FIFO0指针-全局内存变量 */
void **mPendSV_FIFO_P1;                             /*!< FIFO1指针-全局内存变量 */
#endif
volatile s_bool_t m_sign_fifo = true;               /*!< FIFO 互斥访问锁 */

/* 中断挂起服务处理器 */
void mPendSV_FIFOHandler(void)
{
	register void **p;
	register void *sv;
	#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
	register s_u32_t depth;
	#endif
	
__LABLE:
	m_sign_fifo = false;
	/* 独占访问FIFO0 */
	{
		#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
		depth = (s_u32_t)(mPendSV_FIFO_P0 - mPendSV_FIFO[0]);
		if(depth > mPendSV_FIFO_DepthMAX){
			mPendSV_FIFO_DepthMAX = depth;
		}
		#endif
		p = mPendSV_FIFO[0];
		do{
			sv = *++p;
			(*sPendSV_FIFOHandler[*(const s_u8_t *)sv])(sv);
		}while(mPendSV_FIFO_P0 > p);
		mPendSV_FIFO_P0 = mPendSV_FIFO[0];
	}
	m_sign_fifo = true;
	if(mPendSV_FIFO_P1 == mPendSV_FIFO[1]) return;
	/* 独占访问FIFO1 */
	{
		#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
		depth = (s_u32_t)(mPendSV_FIFO_P1 - mPendSV_FIFO[1]);
		if(depth > mPendSV_FIFO_DepthMAX){
			mPendSV_FIFO_DepthMAX = depth;
		}
		#endif
		p = mPendSV_FIFO[1];
		do{
			sv = *++p;
			(*sPendSV_FIFOHandler[*(const s_u8_t *)sv])(sv);
		}while(mPendSV_FIFO_P1 > p);
		mPendSV_FIFO_P1 = mPendSV_FIFO[1];
	}
	if(mPendSV_FIFO_P0 > mPendSV_FIFO[0]) goto __LABLE;
}

#endif



#endif
